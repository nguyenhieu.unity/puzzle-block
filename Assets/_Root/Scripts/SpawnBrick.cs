using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBrick : MonoBehaviour
{
    [SerializeField] List<GameObject> prefabs = new List<GameObject>();

    [SerializeField] MapController mapController;

    [SerializeField] Transform initPosition;

    [SerializeField] List<GameObject> listSpawn = new List<GameObject>();
    private void Awake()
    {
        mapController = FindObjectOfType<MapController>();

        InitPrefabRandom();
    }
    private void InitPrefabRandom()
    {   

        for (int i = 0; i < 3; i++)
        {   
            int rand = Random.Range(0, prefabs.Count);
           
            GameObject ob = Instantiate(prefabs[rand], initPosition);

            ob.transform.position = mapController.transformInitialPoint[i].position;

            ob.GetComponent<Brick>().idSpawn = i;

            ob.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            listSpawn.Add(ob);
        }
    }

    public void RemoveSpawnList(GameObject ob)
    {
        listSpawn.Remove(ob);

        if (listSpawn.Count == 0)
        {
            InitPrefabRandom();
        }
    }
    
}
