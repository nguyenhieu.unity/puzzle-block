﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public GameObject shadowOb;

    GameObject _shadowOb;

    public Collider2D boxCollider;

    public MapController mapController;

    public int idSpawn;

    


    private void Awake()
    {
        mapController = FindObjectOfType<MapController>();
        boxCollider = GetComponent<Collider2D>();

    }
    private void Start()
    {
        _shadowOb = Instantiate(shadowOb, transform);
     
        _shadowOb.SetActive(false);
        SetUpBrick();
    }

    private void SetUpBrick()
    {
        boxCollider.enabled = true;
    }

    private void DeActiveBrick()
    {
        boxCollider.enabled = false;

        mapController.spawnBrick.RemoveSpawnList(gameObject);
    }
    private void OnMouseDrag()
    {
        
        Vector3 mousePosWorld = GetMousePos();
        gameObject.transform.position = mousePosWorld;
        _shadowOb.SetActive(true);
        _shadowOb.transform.position = Utilities.Instance.ConvertToGrid(mousePosWorld);

        gameObject.transform.localScale = new Vector3(1, 1, 1);

        SetLayer();
    }
    private Vector3 GetMousePos()
    {
        Vector3 mousePos = Input.mousePosition;

        mousePos.z = 10;

        return Camera.main.ScreenToWorldPoint(mousePos);

    }

    public int timeDrop = 0;

    public List<int> rowid = new List<int>();

    public List<int> colid = new List<int>();
    private void OnMouseUp()
    { 
        
        if (mapController.CanDrop(_shadowOb.transform))
        {
            transform.position = _shadowOb.transform.position;
            
            _shadowOb.SetActive(false);
        // Điền ô đã có block 
            for (int i = 0; i <_shadowOb.transform.childCount ; i++)
            {   
                //  vị trí nguyên của local position của từng ô bóng
                Vector3Int posInt = Utilities.Instance.ConvertToGrid( mapController.transform.InverseTransformPoint( _shadowOb.transform.GetChild(i).position));

                // add từng ô của block đang sử dụng vào trong grid 
                mapController.blockChild[posInt.y, posInt.x] = transform.GetChild(i).gameObject;
                // đánh dấu là ô taị grid có block đã bị chiếm
                mapController.gridBrick[posInt.y, posInt.x].isOccupied = true;
            }
            Vector3Int posCheckInt = Utilities.Instance.ConvertToGrid(mapController.transform.InverseTransformPoint(_shadowOb.transform.position));

            //Nếu các cột đều có block thì xóa block và trả lại trạng thái rỗng cho grid
       
            for (int i = 0; i < 8; i++)  //
            {
                if (mapController.CheckColumnClear(i))
                {
                    
                    colid.Add(i);
                  
                }

                if (mapController.CheckRowClear(i))
                {
                    // tra ra hang thu may
                    rowid.Add(i);
                }
            }
            for (int j = 0; j < rowid.Count; j++)
            {
                for (int i = 0; i < 8; i++)
                {    
                  // add những ô trong những hàng  full vào ds xóa
                   mapController.listClearTile.Add(  mapController.blockChild[ rowid[j],i].gameObject);

                    //trả lại trạng thái rỗng cho những ô bị xóa
                    mapController.gridBrick[rowid[j], i].isOccupied = false;
                }
            }
            for (int j = 0; j < colid.Count; j++)
            {
                for (int i = 0; i < 8; i++)
                { // add những ô trong những cột  full vào ds xóa
                    mapController.listClearTile.Add(  mapController.blockChild[i,colid[j]].gameObject);
                  
                //trả lại trạng thái rỗng cho những ô bị xóa
                  mapController.gridBrick[i, colid[j]].isOccupied = false;

                }
            }
            DeActiveBrick();
            ClearAllTile();
            ResetLayer();
        }
        else
        {

            gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            transform.position = mapController.transformInitialPoint[idSpawn].position;
            _shadowOb.SetActive(false);
        }

    }

    private void ClearAllTile()
    {
        if (mapController.listClearTile.Count > 0)
        {
            for (int i = 0; i < mapController.listClearTile.Count; i++)
            {
                Destroy(mapController.listClearTile[i]);
            }
            
            mapController.listClearTile.Clear();
        }
    }
    private void SetLayer()
    {
        gameObject.transform.SetParent(mapController.tempParent);
    }
    private void ResetLayer()
    {
        gameObject.transform.SetParent(mapController.transform);
    }

}


