using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
// using UnityEditor.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    [Header("Text")]
    [SerializeField] TextMeshProUGUI textScore;

    [SerializeField] GameObject GameOverUI;

    private void Awake()
    {
        instance = this;
    }
    public void GameOver()
    {
        Debug.Log("Game OVER");
        GameOverUI.SetActive(true);
        // SceneManager.LoadScene("GameOver");
    }

    public void Score()
    {
        textScore.text = ("Score: " + GameController.instance._score).ToString();
    }

    private void Update()
    {
        Score();
    }

    public void LoadGameRestart()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
