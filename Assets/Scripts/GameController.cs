using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public static GameController instance;

    [Header("Span Block")]
    public GameObject[] BlockPrefabs;

    public Transform[] trnWait;

    public GameObject[] objSelect;
    [Header("Score")]
    public int _score = 0;

    [Header("Enums")]
    public Enums.StatusGame enumStatusGame;

    private void Awake()
    {
        instance = this;
    }
    [ContextMenu("random")]
    private void Start()
    {
        objSelect = new GameObject[3];
        SetBlockWaitPlayer();
    }

    public void SetBlockWaitPlayer()
    {
        int _t = trnWait.Length;
        int _z = -1;
        while (_t > 0)
        {
            int _rd = Random.Range(0, BlockPrefabs.Length);
            if (_rd != _z)
            {
                // Debug.Log(X);
                GameObject newObj = Instantiate(BlockPrefabs[_rd], trnWait[_t - 1].position, Quaternion.identity);

                objSelect[_t - 1] = newObj;
                _z = _rd;
                _t = _t - 1;
            }
        }
        amountBlock = 3;

        // Debug.Log("1+");

    }

    int amountBlock = 3;

    [ContextMenu("Test Amount")]
    public void SpawnBlockWait()//? Kiem tra xem cac vi tri cho de nguoi choi keo da het chua -> Neu het thi spawn ra cai moi
    {

        for (int i = 0; i < objSelect.Length; i++)
        {
            if (objSelect[i] != null)
            {
                if (objSelect[i].GetComponent<Block>().poly.enabled == false)
                {
                    objSelect[i] = null;
                    amountBlock = amountBlock - 1;
                }
            }
        }
        if (amountBlock <= 0)
        {
            amountBlock = 0;
            SetBlockWaitPlayer();
        }
    }

}
