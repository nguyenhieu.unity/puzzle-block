using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public GameObject tileBase;

    public static int widthBoard = 8, heigtBoard = 8;

    public TileBase[,] gridBrick = new TileBase[widthBoard, heigtBoard];

    public GameObject[,] blockChild = new GameObject[widthBoard, heigtBoard];

    public List<Transform> transformInitialPoint;

    public SpawnBrick spawnBrick;

    public List<GameObject> listClearTile = new List<GameObject>();

    public Transform tempParent;
    private void Awake()
    {
        spawnBrick = GetComponent<SpawnBrick>();
    }
    private void Start()
    {
        CreateMap();

        transform.position = Utilities.Instance.ConvertToGrid(new Vector3(0, -Camera.main.orthographicSize / 2 - 1f, 0));
    }
    private void CreateMap()
    {
        for (int i = 0; i < heigtBoard; i++)
        {
            for (int j = 0; j < widthBoard; j++)
            {
                GameObject tile = Instantiate(tileBase, transform);

                tile.name = $"[{i},{j}]";
                gridBrick[i, j] = tile.gameObject.GetComponent<TileBase>();
                tile.transform.localPosition = new Vector3(j, i, 0);
            }
        }


    }

    public bool CanDrop(Transform parentTrans)
    {
        for (int i = 0; i < parentTrans.childCount; i++)
        {
            Vector3Int posIntChecked = Utilities.Instance.ConvertToGrid(transform.InverseTransformPoint(parentTrans.GetChild(i).position));
            if (posIntChecked.x > 7 || posIntChecked.y < 0 || posIntChecked.x < 0 || posIntChecked.y > 7)
            {
                return false;
            }
            else if (gridBrick[posIntChecked.y, posIntChecked.x].isOccupied == true)
            {
                return false;
            }

        }
        return true;
    }


    public bool CheckRowClear(int col)
    {
          
            for (int j = 0; j < 8; j++)
            {
                if (gridBrick[col,j].isOccupied != true)
                {
                    return false;
                }
            }
        return true;

    }

    public bool CheckColumnClear(int row)
    {
       
        for (int j = 0; j < 8; j++)
        {
            if (gridBrick[j, row].isOccupied != true)
            {
                return false;
            }
        }
        return true;
    }

}
