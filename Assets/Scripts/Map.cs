using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Map : MonoBehaviour
{
    [SerializeField] int WIDTH = 8, HEIGHT = 8;

    [SerializeField] Transform[,] trnTileInMap;
    [SerializeField] GameObject[,] listBlockChild;


    public Vector2Int[] locChild;
    private void Start()
    {
        trnTileInMap = new Transform[WIDTH, HEIGHT];
        listBlockChild = new GameObject[WIDTH, HEIGHT];
        TransformTileMap();
    }


    // Gop cac phan tu o trong trong map vao Transform de quan li
    public void TransformTileMap()
    {
        for (int k = 0; k < transform.childCount; k++)
        {
            Vector2Int a = Vector2Int.RoundToInt(gameObject.transform.GetChild(k).position);
            trnTileInMap[a.x, a.y] = gameObject.transform.GetChild(k).transform;
        }

    }

    public bool CheckChildrenInTile(Vector2Int trns)// kiem tra xem toa do GameObject chuyen vao da co can chua 
    {
        if (trnTileInMap[trns.x, trns.y].gameObject.GetComponent<TileInMap>()._isStatus == true)
        {
            return true;// co roi
        }
        return false;
    }

    public void SetStatusTile(Vector2Int pos, bool _st)// truyen vao _st de co the set trang thai cua tile trong map
    {
        trnTileInMap[pos.x, pos.y].gameObject.GetComponent<TileInMap>()._isStatus = _st;
    }


    // Add block con vao mang quan li
    public void AddBlockChild(Vector2Int posChild, GameObject trnObj)
    {
        if (listBlockChild[posChild.x, posChild.y] == null)
        {
            listBlockChild[posChild.x, posChild.y] = trnObj;
        }
    }
    [SerializeField] int _isStatusGame = 0;

    // Kiem tra giong nhau de xoa 
    public void CheckListBlockChild()
    {
        Debug.Log("------------------------------------");
        //@ An diem 

        List<int> rows = new List<int>();
        List<int> colums = new List<int>();

        for (int i = 0; i < 8; i++)
        {
            Debug.Log("i = " + i + " Colum " + CheckColumsListBlock(i) + " Row = " + CheckRowsListBlock(i));

            if (CheckColumsListBlock(i))//kiem tra cot -> xem cac hang thay doi -> _is = true
            {
                colums.Add(i);
            }
            if (CheckRowsListBlock(i))
            {
                rows.Add(i);
            }
        }
        //? Xoa hang, cot trung nhau
        for (int i = 0; i < colums.Count; i++)
        {
            RemoveBlockChild(colums[i], true);
        }
        for (int i = 0; i < rows.Count; i++)
        {
            RemoveBlockChild(rows[i], false);
        }
        rows.Clear();
        colums.Clear();

        rows.Clear();


        //? Kiem tra xem ban thang hay thua 

        // Set block wait player
        //@ Sinh ra block wait player 
        GameController.instance.SpawnBlockWait();

        //@ Kiem tra thang thua tu dong 
        AutomaticGame();
    }



    public void RemoveBlockChild(int d, bool _is)//xoa d hang _ cot d 
    {
        if (_is)// xoa cot d
        {
            for (int i = 0; i < 8; i++)
            {
                //@ truoc khi xoa cac phan tu thi can phai dat cac o tile tren Map _isStatus = false
                trnTileInMap[i, d].gameObject.GetComponent<TileInMap>()._isStatus = false;

                if (listBlockChild[i, d] != null)
                {
                    listBlockChild[i, d].GetComponent<Animator>().SetTrigger("DestroyAnimation");//Animation
                    
                    Destroy(listBlockChild[i, d].gameObject, 0.4f);

                    listBlockChild[i, d] = null;
                    // Cong diem 
                    GameController.instance._score = GameController.instance._score + 1;
                }
            }
        }
        else// xoa hang d
        {
            for (int i = 0; i < 8; i++)
            {

                //@ truoc khi xoa cac phan tu thi can phai dat cac o tile tren Map _isStatus = false
                trnTileInMap[d, i].gameObject.GetComponent<TileInMap>()._isStatus = false;
                if (listBlockChild[d, i] != null)
                {
                    listBlockChild[d, i].GetComponent<Animator>().SetTrigger("DestroyAnimation");//Animation

                    Destroy(listBlockChild[d, i].gameObject, 0.4f);
                    listBlockChild[d, i] = null;

                    // Cong diem 
                    GameController.instance._score = GameController.instance._score + 1;

                }
            }
        }
    }


    public bool CheckColumsListBlock(int colum)// kiem tra cot -> Duyet tung hang 
    {
        for (int j = 0; j < 8; j++)
        {
            if (listBlockChild[j, colum] == null)
            {
                return false;
            }
        }
        return true;// thoa man deu co o tren tile map 
    }

    public bool CheckRowsListBlock(int row)// kiem tra hang -> Duyet tung cot 
    {
        for (int j = 0; j < 8; j++)
        {
            if (listBlockChild[row, j] == null)
            {
                return false;
            }
        }
        return true;// thoa man deu co o tren tile map 
    }


    #region AI Automatic Win Lose

    public void AutomaticGame()
    {
        // Debug.Log("2");
        for (int i = 0; i < 3; i++)
        {

            if (GameController.instance.objSelect[i] != null)
            {
                GameObject newBlock = Instantiate(GameController.instance.objSelect[i], Vector3.zero, GameController.instance.objSelect[i].transform.rotation);
                newBlock.SetActive(false);
                newBlock.transform.localScale = new Vector3(1, 1, 1);
                _isStatusGame = 0;
                CheckStatusGame(newBlock);

                if (_isStatusGame == 1)
                {
                    break;
                }
            }
        }

        if (_isStatusGame == 0)
        {
            //Set status Game
            GameController.instance.enumStatusGame = Enums.StatusGame.Over;

            // Game Over
            UIController.instance.GameOver();
        }
        else
        {
            Debug.Log("Next");
            _isStatusGame = 0;
        }
    }

    [ContextMenu("Set Win")]
    public void CheckStatusGame(GameObject objBlock)// truyen vao so phan tu cua Block can xet 
    {
        locChild = new Vector2Int[objBlock.transform.childCount];
        // Debug.Log(locChild.Length);
        // Khoi tao mang
        for (int i = 0; i < locChild.Length; i++)
        {
            // Debug.Log(objBlock.transform.GetChild(i).position);
            locChild[i] = Vector2Int.RoundToInt(objBlock.transform.GetChild(i).position);// lay ra toa do cua cac vi tri 
                                                                                         // -> Do lech = Vecto locChild[i]
        }

        // Load xong objBlock dua vao 
        if (Check(objBlock))
        {
            _isStatusGame = 1;
        }
    }

    public bool Check(GameObject objBlock)// kiem tra xem trong cac Block con lai co Block nao dat duoc hay khong
    {
        if (GetMold(objBlock))
        {
            return true;// next -> van tiep tuc Game 
        }
        else
        {
            //? Lose -> Dung Game -> Khong co vi tri nao dat duoc block con lai
            return false;
        }
    }
    // tim tile lam moc 
    public bool GetMold(GameObject objBlock)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                // trnTileInMap[i, j] -> duoc lay lam goc 
                if (listBlockChild[i, j] == null)
                {
                    // Xet
                    Vector2Int vt = new Vector2Int(i, j);
                    // Debug.Log(vt + " : ");
                    if (Set(vt, objBlock) == false)// da co vi tri thoa man 
                    {
                        Destroy(objBlock);//!

                        return true;// co the xep duoc Block A vao Map
                    }
                }
            }
        }
        Destroy(objBlock);//!
        return false;// khong the xep duoc Block A vao Map

    }

    public bool Set(Vector2Int vt, GameObject objBlock)
    {
        // Debug.Log("***********************");
        // Debug.Log("goc = " + vt);


        for (int i = 0; i < objBlock.transform.childCount; i++)
        {
            Vector2Int pos = vt + locChild[i];

            if (pos.x < 0 || pos.x > 7 || pos.y < 0 || pos.y > 7)
            {
                // Debug.Log(pos + " KTM ");
                return true;
            }
            if (listBlockChild[pos.x, pos.y] != null)
            {
                // Debug.Log(pos + " KTM ");
                return true;// da co phan tu bi chan -> khong dat vao duoc 
            }
        }
        // Debug.Log("TM");
        return false;// dat vao duoc 
    }
    #endregion

}
