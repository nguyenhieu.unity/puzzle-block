using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class Block : MonoBehaviour
{

    [SerializeField] Vector2 transformStart;
    [SerializeField] Vector2 VtParent;// Vector cha dieu khien ca Block

    [SerializeField] GameObject Silhouette;// hinh bong

    public PolygonCollider2D poly;


    // Di chuyen Block Parent
    Vector3 mOffset;
    float mZCoord;

    [Header("Other Layer")]

    private const int SortingOrderHover = 10;
    private const int SortingOrderDefault = 1;

    [SerializeField] GameObject map;

    private void Start()
    {
        transformStart = transform.position;
        map = GameObject.Find("Map");
        Rotate();
        SetScale(false);

        // set layer
        SetLayer(SortingOrderDefault);

    }
    public void SetLayer(int _layer)
    {
        foreach (Transform child in transform)
        {
            SpriteRenderer spriteRenderer = child.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null)
            {
                spriteRenderer.sortingOrder = _layer;
            }
        }
    }

    public void Rotate()
    {
        int[] rotateAngle = { 0, 90, 180, 270 };

        transform.rotation = Quaternion.Euler(0, 0, rotateAngle[Random.Range(0, 4)]);
    }


    #region Di chuyen Block Parent
    private void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        mOffset = gameObject.transform.position - GetMouseWorldPos();
        SetScale(true);

        //Set layer
        SetLayer(SortingOrderHover);

        // 
        // Silhouette.SetActive(true);
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = mZCoord;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }
    private void OnMouseDrag()
    {
        transform.position = GetMouseWorldPos() + mOffset;

    }

    #endregion
    Vector2 offset;
    public void SetIntPosition()
    {
        Vector2 childStart = gameObject.transform.GetChild(0).position;// vi tri con dau hien tai
        Vector2Int childPoint = SetPositionInt(gameObject.transform.GetChild(0).position);// vi tri lam tron phan tu con dau lam mau

        offset = childStart - childPoint;// do lech vi tri 
    }

    // Set lai toa do cua Block Parent
    [Obsolete]
    private void OnMouseUp()
    {
        // Set Layer
        SetLayer(SortingOrderDefault);

        SetScale(true);

        SetIntPosition();


        //? Kiem tra xem cac phan tu con cua no co thoa man khong 

        if (IsChildren())// Khong thoa man de duyet bock vao map 
        {
            transform.position = transformStart;// Di chuyen ve vi tri ban dau
            // tra lai scale ban dau 
            SetScale(false);
        }
        else// thoa man de duyet block vao map
        {
            transform.position = transform.position - new Vector3(offset.x, offset.y);

            //? -> Them no vao 1 cai list de tu do de quan li khi du thi xoa
            for (int i = 0; i < transform.childCount; i++)
            {
                Vector2Int pos = SetPositionInt(gameObject.transform.GetChild(i).position);

                // Set cho cac o trong map se chuyen sang _isStatus true -> Co block Child nam tren no
                map.GetComponent<Map>().SetStatusTile(pos, true);// true -> Da co gach tren no -> _isStatus= true;

                //@ Them Object Block Child vao trong List xoa 

                map.GetComponent<Map>().AddBlockChild(pos, transform.GetChild(i).gameObject);
            }

            // Set lai trang thai collider de khong di chuyen duoc nua
            if (poly == null)
            {
                poly = gameObject.GetComponent<PolygonCollider2D>();
            }
            poly.enabled = false;

            //@ Kiem tra xoa hang, cot da du so luong 
            map.GetComponent<Map>().CheckListBlockChild();
        }
    }


    public bool IsChildren()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)// kiem tra tat ca cac phan tu con
        {
            Vector2Int trnChild = SetPositionInt(gameObject.transform.GetChild(i).position);// chuyen toa do cua child sang so nguyen -> Xet tranform no de 
            if (trnChild.x < 0 || trnChild.x > 7 || trnChild.y < 0 || trnChild.y > 7)
            {
                return true; // -> di chuyen ve vi tri ban dau
            }
            else
            {
                if (IsChildrenInTile(trnChild))
                {
                    return true;// da co -> khong thoa man
                }
            }
        }
        return false;// thoa man -> Set lai vi tri cua BlockParent
    }

    private bool IsChildrenInTile(Vector2Int trns)
    {
        if (map.GetComponent<Map>().CheckChildrenInTile(trns))// da co -> Khong thoa man
        {
            return true;
        }
        return false;// thoa man 
    }

    public Vector2Int SetPositionInt(Vector2 Vt)// tra ve gia tri vector sau khi lam tron
    {
        Vector2Int VtLocal = Vector2Int.RoundToInt(Vt);
        return VtLocal;
    }

    // Set scale cua Object
    public void SetScale(bool isScale)
    {
        if (isScale)
        {
            gameObject.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            gameObject.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        }
    }



    // Kiem tra xem co phu hop de dua GameObject len map khong

    private void Update()
    {



    }
    private void LateUpdate()
    {

        if (transform.childCount <= 0)
        {
            Destroy(gameObject);
        }
        if (GameController.instance.enumStatusGame == Enums.StatusGame.Over)
        {
            poly.enabled = false;
        }


        if (poly.enabled == false)
        {
            Destroy(Silhouette);
        }
        else
        {
            if (transform.position.x > -0.5f && transform.position.x <= 7 && transform.position.y > -0.5f && transform.position.y <= 7)
            {
                Silhouette.SetActive(true);
                Vector3Int trnSi = Vector3Int.RoundToInt(transform.position);
                Silhouette.transform.position = trnSi;
            }
            else
            {
                Silhouette.SetActive(false);
            }
        }
    }
}